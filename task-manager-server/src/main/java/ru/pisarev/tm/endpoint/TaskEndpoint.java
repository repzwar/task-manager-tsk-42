package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IProjectTaskService;
import ru.pisarev.tm.api.service.ITaskService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.dto.SessionDTO;
import ru.pisarev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint {

    private ITaskService taskService;

    private IProjectTaskService projectTaskService;

    public TaskEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ITaskService taskService,
            @NotNull final IProjectTaskService projectTaskService
    ) {
        super(serviceLocator);
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @WebMethod
    public List<TaskDTO> findTaskAll(@NotNull @WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    public void addTaskAll(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "collection") final Collection<TaskDTO> collection
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public TaskDTO addTask(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "task") final TaskDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.add(session.getUserId(), entity);
    }

    @WebMethod
    public TaskDTO addTaskWithName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearTask(@WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "task") final TaskDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeTaskByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        taskService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskDTO updateTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public TaskDTO updateTaskByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public TaskDTO startTaskById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @WebMethod
    public TaskDTO startTaskByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskDTO startTaskByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public TaskDTO finishTaskById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public TaskDTO finishTaskByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public TaskDTO finishTaskByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return taskService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public List<TaskDTO> findTaskByProjectId(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectTaskService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        projectTaskService.bindTaskById(session.getUserId(), taskId, projectId);
    }

    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "taskId") final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        projectTaskService.unbindTaskById(session.getUserId(), taskId);
    }

}
