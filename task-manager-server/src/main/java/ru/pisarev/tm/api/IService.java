package ru.pisarev.tm.api;

import ru.pisarev.tm.dto.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {

}