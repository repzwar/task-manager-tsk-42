package ru.pisarev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.pisarev.tm.dto.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    UserDTO findByLogin(final String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    UserDTO findByEmail(final String email);

    @Delete("DELETE FROM tm_user WHERE login = #{login}")
    void removeUserByLogin(final String login);

    @Insert("INSERT INTO tm_user" +
            "(id, email, login, role, locked, first_name, last_name, middle_name, password_hash)" +
            "VALUES(#{id},#{email},#{login},#{role},#{locked}," +
            "#{first_name},#{last_name},#{middle_name},#{password_hash})")
    void add(
            @Param("id") String id,
            @Param("email") String email,
            @Param("login") String login,
            @Param("role") String role,
            @Param("locked") boolean locked,
            @Param("first_name") String firstName,
            @Param("last_name") String lastName,
            @Param("middle_name") String middleName,
            @Param("password_hash") String passwordHash
    );

    @Update("UPDATE tm_user" +
            "SET email=#{email}, login=#{login}, role=#{role}, locked=#{locked}, password_hash=#{password_hash}" +
            "first_name=#{first_name}, last_name=#{last_name}, middle_name=#{middle_name} WHERE id=#{id}")
    void update(
            @Param("id") String id,
            @Param("email") String email,
            @Param("login") String login,
            @Param("role") String role,
            @Param("locked") boolean locked,
            @Param("first_name") String firstName,
            @Param("last_name") String lastName,
            @Param("middle_name") String middleName,
            @Param("password_hash") String passwordHash
    );

    @Select("SELECT * FROM tm_user")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    List<UserDTO> findAll();

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    UserDTO findById(final String id);

    @Delete("DELETE * FROM tm_user")
    void clear();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(final String id);

}
