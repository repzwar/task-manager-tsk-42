package ru.pisarev.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.pisarev.tm.endpoint package.
 * &lt;p&gt;An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CloseAllByUserId_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "closeAllByUserId");
    private final static QName _CloseAllByUserIdResponse_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "closeAllByUserIdResponse");
    private final static QName _FindAllByUserId_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "findAllByUserId");
    private final static QName _FindAllByUserIdResponse_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "findAllByUserIdResponse");
    private final static QName _LockByLogin_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "lockByLogin");
    private final static QName _LockByLoginResponse_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "lockByLoginResponse");
    private final static QName _RemoveByLogin_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "removeByLogin");
    private final static QName _RemoveByLoginResponse_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "removeByLoginResponse");
    private final static QName _UnlockByLogin_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "unlockByLogin");
    private final static QName _UnlockByLoginResponse_QNAME = new QName("http://endpoint.tm.pisarev.ru/", "unlockByLoginResponse");
    private final static QName _SessionTimestamp_QNAME = new QName("", "timestamp");
    private final static QName _SessionUserId_QNAME = new QName("", "userId");
    private final static QName _SessionSignature_QNAME = new QName("", "signature");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.pisarev.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CloseAllByUserId }
     */
    public CloseAllByUserId createCloseAllByUserId() {
        return new CloseAllByUserId();
    }

    /**
     * Create an instance of {@link CloseAllByUserIdResponse }
     */
    public CloseAllByUserIdResponse createCloseAllByUserIdResponse() {
        return new CloseAllByUserIdResponse();
    }

    /**
     * Create an instance of {@link FindAllByUserId }
     */
    public FindAllByUserId createFindAllByUserId() {
        return new FindAllByUserId();
    }

    /**
     * Create an instance of {@link FindAllByUserIdResponse }
     */
    public FindAllByUserIdResponse createFindAllByUserIdResponse() {
        return new FindAllByUserIdResponse();
    }

    /**
     * Create an instance of {@link LockByLogin }
     */
    public LockByLogin createLockByLogin() {
        return new LockByLogin();
    }

    /**
     * Create an instance of {@link LockByLoginResponse }
     */
    public LockByLoginResponse createLockByLoginResponse() {
        return new LockByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveByLogin }
     */
    public RemoveByLogin createRemoveByLogin() {
        return new RemoveByLogin();
    }

    /**
     * Create an instance of {@link RemoveByLoginResponse }
     */
    public RemoveByLoginResponse createRemoveByLoginResponse() {
        return new RemoveByLoginResponse();
    }

    /**
     * Create an instance of {@link UnlockByLogin }
     */
    public UnlockByLogin createUnlockByLogin() {
        return new UnlockByLogin();
    }

    /**
     * Create an instance of {@link UnlockByLoginResponse }
     */
    public UnlockByLoginResponse createUnlockByLoginResponse() {
        return new UnlockByLoginResponse();
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseAllByUserId }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CloseAllByUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "closeAllByUserId")
    public JAXBElement<CloseAllByUserId> createCloseAllByUserId(CloseAllByUserId value) {
        return new JAXBElement<CloseAllByUserId>(_CloseAllByUserId_QNAME, CloseAllByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseAllByUserIdResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CloseAllByUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "closeAllByUserIdResponse")
    public JAXBElement<CloseAllByUserIdResponse> createCloseAllByUserIdResponse(CloseAllByUserIdResponse value) {
        return new JAXBElement<CloseAllByUserIdResponse>(_CloseAllByUserIdResponse_QNAME, CloseAllByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByUserId }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindAllByUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "findAllByUserId")
    public JAXBElement<FindAllByUserId> createFindAllByUserId(FindAllByUserId value) {
        return new JAXBElement<FindAllByUserId>(_FindAllByUserId_QNAME, FindAllByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByUserIdResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindAllByUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "findAllByUserIdResponse")
    public JAXBElement<FindAllByUserIdResponse> createFindAllByUserIdResponse(FindAllByUserIdResponse value) {
        return new JAXBElement<FindAllByUserIdResponse>(_FindAllByUserIdResponse_QNAME, FindAllByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LockByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "lockByLogin")
    public JAXBElement<LockByLogin> createLockByLogin(LockByLogin value) {
        return new JAXBElement<LockByLogin>(_LockByLogin_QNAME, LockByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LockByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "lockByLoginResponse")
    public JAXBElement<LockByLoginResponse> createLockByLoginResponse(LockByLoginResponse value) {
        return new JAXBElement<LockByLoginResponse>(_LockByLoginResponse_QNAME, LockByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RemoveByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "removeByLogin")
    public JAXBElement<RemoveByLogin> createRemoveByLogin(RemoveByLogin value) {
        return new JAXBElement<RemoveByLogin>(_RemoveByLogin_QNAME, RemoveByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RemoveByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "removeByLoginResponse")
    public JAXBElement<RemoveByLoginResponse> createRemoveByLoginResponse(RemoveByLoginResponse value) {
        return new JAXBElement<RemoveByLoginResponse>(_RemoveByLoginResponse_QNAME, RemoveByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link UnlockByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "unlockByLogin")
    public JAXBElement<UnlockByLogin> createUnlockByLogin(UnlockByLogin value) {
        return new JAXBElement<UnlockByLogin>(_UnlockByLogin_QNAME, UnlockByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link UnlockByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pisarev.ru/", name = "unlockByLoginResponse")
    public JAXBElement<UnlockByLoginResponse> createUnlockByLoginResponse(UnlockByLoginResponse value) {
        return new JAXBElement<UnlockByLoginResponse>(_UnlockByLoginResponse_QNAME, UnlockByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "timestamp", scope = Session.class)
    public JAXBElement<Long> createSessionTimestamp(Long value) {
        return new JAXBElement<Long>(_SessionTimestamp_QNAME, Long.class, Session.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "userId", scope = Session.class)
    public JAXBElement<String> createSessionUserId(String value) {
        return new JAXBElement<String>(_SessionUserId_QNAME, String.class, Session.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "signature", scope = Session.class)
    public JAXBElement<String> createSessionSignature(String value) {
        return new JAXBElement<String>(_SessionSignature_QNAME, String.class, Session.class, value);
    }

}
