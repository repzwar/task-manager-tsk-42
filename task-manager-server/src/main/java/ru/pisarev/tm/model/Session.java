package ru.pisarev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
public class Session extends AbstractBusinessEntity {

    @Column
    @Nullable
    private Long timestamp;

    @Column
    @Nullable
    private String signature;

}
