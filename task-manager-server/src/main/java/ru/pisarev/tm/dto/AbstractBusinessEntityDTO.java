package ru.pisarev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@MappedSuperclass
public abstract class AbstractBusinessEntityDTO extends AbstractEntityDTO {

    @Nullable
    @Column(name = "user_id")
    protected String userId;

}
