package ru.pisarev.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.service.*;
import ru.pisarev.tm.component.Backup;
import ru.pisarev.tm.dto.ProjectDTO;
import ru.pisarev.tm.dto.TaskDTO;
import ru.pisarev.tm.endpoint.*;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.service.*;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.pisarev.tm.util.SystemUtil.getPID;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, userService, propertyService);

    @NotNull
    private final DataService dataService = new DataService(userService, taskService, projectService, sessionService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectService, projectTaskService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionService, userService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskService, projectTaskService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this, userService, sessionService);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this, dataService);

    public void start(String... args) {
        displayWelcome();
        process();
        //backup.init();
        initEndpoint();
    }

    public void initApplication() {
        initPID();
    }

    private void initData() {
        @NotNull final String adminId = userService.add("admin", "admin", "admin@a").getId();
        userService.findByLogin("admin").setRole(Role.ADMIN);
        @NotNull final String userId = userService.add("user", "user").getId();

        projectService.add(adminId, new ProjectDTO("Project C", "-")).setStatus(Status.COMPLETED);
        projectService.add(adminId, new ProjectDTO("Project A", "-"));
        projectService.add(adminId, new ProjectDTO("Project B", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(adminId, new ProjectDTO("Project D", "-")).setStatus(Status.COMPLETED);
        taskService.add(adminId, new TaskDTO("Task C", "-")).setStatus(Status.COMPLETED);
        taskService.add(adminId, new TaskDTO("Task A", "-"));
        taskService.add(adminId, new TaskDTO("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(adminId, new TaskDTO("Task D", "-")).setStatus(Status.COMPLETED);
        taskService.add(userId, new TaskDTO("Task B2", "-")).setStatus(Status.IN_PROGRESS);
    }

    public void initEndpoint() {
        initEndpoint(projectEndpoint);
        initEndpoint(sessionEndpoint);
        initEndpoint(taskEndpoint);
        initEndpoint(adminEndpoint);
        initEndpoint(dataEndpoint);
    }

    public void initEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";

    }

}
