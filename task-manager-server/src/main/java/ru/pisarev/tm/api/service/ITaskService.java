package ru.pisarev.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.dto.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<TaskDTO> {

    TaskDTO findByName(String userId, String name);

    TaskDTO findByIndex(String userId, Integer index);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, Integer index);

    TaskDTO updateById(String userId, final String id, final String name, final String description);

    TaskDTO updateByIndex(String userId, final Integer index, final String name, final String description);

    TaskDTO startById(String userId, String id);

    TaskDTO startByIndex(String userId, Integer index);

    TaskDTO startByName(String userId, String name);

    TaskDTO finishById(String userId, String id);

    TaskDTO finishByIndex(String userId, Integer index);

    TaskDTO finishByName(String userId, String name);

    TaskDTO add(String userId, String name, String description);

    @NotNull
    @SneakyThrows
    List<TaskDTO> findAll(@NotNull String userId);

    @SneakyThrows
    void addAll(@NotNull String userId, @Nullable Collection<TaskDTO> collection);

    @Nullable
    @SneakyThrows
    TaskDTO add(@NotNull String userId, @Nullable TaskDTO entity);

    @Nullable
    @SneakyThrows
    TaskDTO findById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void clear(@NotNull String userId);

    @SneakyThrows
    void removeById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void remove(@NotNull String userId, @Nullable TaskDTO entity);
}
