package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.ISessionService;
import ru.pisarev.tm.api.service.IUserService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.dto.SessionDTO;
import ru.pisarev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint {

    private ISessionService sessionService;

    private IUserService userService;

    public SessionEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService
    ) {
        super(serviceLocator);
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @WebMethod
    public SessionDTO open(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    public void close(@WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session);
        sessionService.close(session);
    }

    @WebMethod
    public SessionDTO register(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        userService.add(login, password, email);
        return sessionService.open(login, password);
    }

    @WebMethod
    public UserDTO setPassword(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public UserDTO updateUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
