package ru.pisarev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
public class Task extends AbstractBusinessEntity {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    private Project project;

    @Nullable
    @Column(name = "start_date")
    private Date startDate;

    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;

    @Column
    @NotNull
    private Date created = new Date();

}
