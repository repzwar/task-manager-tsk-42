package ru.pisarev.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.dto.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<ProjectDTO> {

    ProjectDTO findByName(final String userId, final String name);

    ProjectDTO findByIndex(final String userId, final Integer index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final Integer index);

    ProjectDTO updateById(final String userId, final String id, final String name, final String description);

    ProjectDTO updateByIndex(final String userId, final Integer index, final String name, final String description);

    ProjectDTO startById(final String userId, final String id);

    ProjectDTO startByIndex(final String userId, final Integer index);

    ProjectDTO startByName(final String userId, final String name);

    ProjectDTO finishById(final String userId, final String id);

    ProjectDTO finishByIndex(final String userId, final Integer index);

    ProjectDTO finishByName(final String userId, final String name);

    ProjectDTO add(String userId, String name, String description);

    @NotNull
    @SneakyThrows
    List<ProjectDTO> findAll(@NotNull String userId);

    @SneakyThrows
    void addAll(@NotNull String userId, @Nullable Collection<ProjectDTO> collection);

    @Nullable
    @SneakyThrows
    ProjectDTO add(@NotNull String userId, @Nullable ProjectDTO entity);

    @Nullable
    @SneakyThrows
    ProjectDTO findById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void clear(@NotNull String userId);

    @SneakyThrows
    void removeById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void remove(@NotNull String userId, @Nullable ProjectDTO entity);
}
