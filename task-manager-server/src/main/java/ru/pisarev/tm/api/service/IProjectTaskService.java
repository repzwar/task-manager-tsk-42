package ru.pisarev.tm.api.service;

import ru.pisarev.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectTaskService {

    List<TaskDTO> findTaskByProjectId(String userId, final String projectId);

    void bindTaskById(String userId, final String taskId, final String projectId);

    void unbindTaskById(String userId, final String taskId);

    void removeProjectById(String userId, final String projectId);

    void removeProjectByIndex(String userId, final Integer index);

    void removeProjectByName(String userId, final String name);

}
