package ru.pisarev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.pisarev.tm.dto.ProjectDTO;

import java.sql.Date;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project" +
            "(id, name, description, status, start_date, finish_date, created, user_id)" +
            "VALUES(#{id},#{name},#{description},#{status},#{startDate},#{finishDate},#{created},#{userId})")
    void add(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("startDate") Date startDate,
            @Param("finishDate") Date finishDate,
            @Param("created") Date created,
            @Param("userId") String userId
    );

    @Update("UPDATE tm_project" +
            "SET name=#{name}, description=#{description}, status=#{status}, start_date=#{startDate}," +
            "finish_date=#{finishDate}, created=#{created}, user_id=#{userId} WHERE id = #{id}")
    void update(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("startDate") Date startDate,
            @Param("finishDate") Date finishDate,
            @Param("created") Date created,
            @Param("userId") String userId
    );

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    ProjectDTO findByIdUserId(final String userId, final String id);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearByUserId(final String userId);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    void removeByIdUserId(final String userId, final String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    List<ProjectDTO> findAllByUserId(final String userId);

    @Select("SELECT * FROM tm_project WHERE name=#{name} AND user_id=#{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    ProjectDTO findByName(final String userId, final String name);

    @Select("SELECT * FROM tm_project" + " WHERE user_id=#{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    ProjectDTO findByIndex(final String userId, final int index);

    @Delete("DELETE FROM tm_project" + " WHERE name = #{name} AND user_id = #{userId}")
    void removeByName(final String userId, final String name);

    @Delete("DELETE FROM tm_project" + " WHERE user_id = #{userId}")
    void removeByIndex(final String userId, final int index);

    @Select("SELECT * FROM tm_project")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    List<ProjectDTO> findAll();

    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    ProjectDTO findById(final String id);

    @Delete("DELETE * FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeById(final String id);

}
