package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IProjectService;
import ru.pisarev.tm.api.service.IProjectTaskService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.dto.ProjectDTO;
import ru.pisarev.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint {

    private IProjectService projectService;

    private IProjectTaskService ProjectTaskService;

    public ProjectEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IProjectService projectService,
            @NotNull final IProjectTaskService ProjectTaskService
    ) {
        super(serviceLocator);
        this.projectService = projectService;
        this.ProjectTaskService = ProjectTaskService;
    }

    @WebMethod
    public List<ProjectDTO> findProjectAll(@WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    public void addProjectAll(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "collection") final Collection<ProjectDTO> collection
    ) {
        serviceLocator.getSessionService().validate(session);
        projectService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public ProjectDTO addProject(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "entity") final ProjectDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.add(session.getUserId(), entity);
    }

    @WebMethod
    public ProjectDTO addProjectWithName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearProject(@WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "entity") final ProjectDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        projectService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public ProjectDTO findProjectByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public ProjectDTO updateProjectById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public ProjectDTO updateProjectByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public ProjectDTO startProjectById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @WebMethod
    public ProjectDTO startProjectByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public ProjectDTO startProjectByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public ProjectDTO finishProjectById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public ProjectDTO finishProjectByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public ProjectDTO finishProjectByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        ProjectTaskService.removeProjectById(session.getUserId(), projectId);
    }

    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        ProjectTaskService.removeProjectByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") final SessionDTO session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        ProjectTaskService.removeProjectByName(session.getUserId(), name);
    }
}
