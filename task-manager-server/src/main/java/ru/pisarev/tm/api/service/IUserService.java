package ru.pisarev.tm.api.service;

import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.dto.UserDTO;

public interface IUserService extends IService<UserDTO> {

    UserDTO findByLogin(final String login);

    UserDTO findByEmail(final String email);

    void removeByLogin(final String login);

    UserDTO add(final String login, final String password);

    UserDTO add(final String login, final String password, final String email);

    UserDTO setPassword(final String id, final String password);

    boolean isLoginExist(final String login);

    boolean isEmailExist(final String email);

    UserDTO updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    );

    UserDTO lockByLogin(final String login);

    UserDTO unlockByLogin(final String login);

}
